﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;


namespace BinToImg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        float scale = 1;

        byte[] buffer;
        StreamWriter file;
        Bitmap bmp;

        public MainWindow()
        {
            InitializeComponent();
            file = new StreamWriter("log.txt");
        }


        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            decimal result;
            var box = (TextBox)sender;

            if (!Decimal.TryParse(e.Text, out result))
                e.Handled = true;
        }

        private void ButtonOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".bin";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                TextBoxFile.Text = dlg.FileName;
                file.WriteLine("Open " + dlg.FileName);

                LoadFile_Click(null, null);
            }
        }

        private void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxFile.Text.Length > 0)
            {
                try
                {
                    buffer = File.ReadAllBytes(TextBoxFile.Text);
                    FileSize.Text = buffer.Length.ToString();
                    file.WriteLine("Loaded " + buffer.Length.ToString());
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void ProcessImage_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(TextBoxIHeader.Text)) TextBoxIHeader.Text = "0";
            
            scale = 1;
            if (buffer != null)
            {
                // Ignore header bytes
                buffer = buffer.Skip(Int32.Parse(TextBoxIHeader.Text)).ToArray();
                
                // Ignore footer bytes
                buffer = buffer.Take(buffer.Length - Int32.Parse(TextBoxIFooter.Text)).ToArray();

                FileSize.Text = buffer.Length.ToString();

                bmp = new Bitmap(Int32.Parse(ImageWidth.Text),Int32.Parse(ImageHeight.Text));

                BitArray b = new BitArray(buffer);

                file.WriteLine("Started processing\n" + ((ComboBoxItem)ColorPallete.SelectedItem).Content.ToString() + "\n" + ((ComboBoxItem)ColorSize.SelectedItem).Content.ToString() + "\nPadding: " + TextBoxBitPadding.Text);
                file.WriteLine("Bits to process: " + b.Length.ToString());

                bool alpha = false;

                if (((ComboBoxItem)ColorPallete.SelectedItem).Content.ToString() == "RGBA" || ((ComboBoxItem)ColorPallete.SelectedItem).Content.ToString() == "ARGB")
                    alpha = true;

                int paddingLen = Int16.Parse(TextBoxPadding.Text);
                int paddingInternalLen = Int16.Parse(TextBoxPaddingInternal.Text);
                int startVal = Int16.Parse(TextBoxBitPadding.Text);

                int[] parts = new int[3];
                if(((ComboBoxItem)ColorSize.SelectedItem).Content.ToString()=="Niestandardowy"){
                    try
                    {
                        parts[0] = Int16.Parse(part1.Text); parts[1] = Int16.Parse(part2.Text); parts[2] = Int16.Parse(part3.Text);
                    } 
                    catch(Exception exc)
                    {
                        MessageBox.Show("Błędne wartości podane dla niestandardowej konwersji"); 
                    }
                }

                if (startVal >= buffer.Length)
                    MessageBox.Show("Dane do zignorowania przekraczają rozmiar bufora");

                int pixel = 0;

                for (int i = startVal; i < b.Length;)
                {
                    byte[] arr = new byte[4];

                    int s = 3;
                    if (alpha) s++;

                    for (int j = 0; j < s; j++)
                    {
                        if (i >= b.Length) break;

                        byte tmp = 0;
                        try
                        {
                            switch (((ComboBoxItem)ColorSize.SelectedItem).Content.ToString())
                            {
                                case "4b":
                                    if (b[i])
                                        tmp |= 0x01 | 0x02;
                                    if (b[i + 1])
                                        tmp |= 0x04 | 0x08;
                                    if (b[i + 2])
                                        tmp |= 0x10 | 0x20;
                                    if (b[i + 3])
                                        tmp |= 0x40 | 0x80;
                                    i += 4;
                                    break;

                                case "5b":
                                    if (b[i])
                                        tmp |= 0x01;
                                    if (b[i + 1])
                                        tmp |= 0x02;
                                    if (b[i + 2])
                                        tmp |= 0x04;
                                    if (b[i + 3])
                                        tmp |= 0x08;
                                    if (b[i + 4])
                                        tmp |= 0x10;
                                    i += 5;
                                    tmp = (byte)(tmp * (255 / 31));
                                    break;

                                case "6b":
                                    if (b[i])
                                        tmp |= 0x01;
                                    if (b[i + 1])
                                        tmp |= 0x02;
                                    if (b[i + 2])
                                        tmp |= 0x04;
                                    if (b[i + 3])
                                        tmp |= 0x08;
                                    if (b[i + 4])
                                        tmp |= 0x10;
                                    if (b[i + 5])
                                        tmp |= 0x20;
                                    i += 6;
                                    tmp = (byte)(tmp * (255 / 63));
                                    break;

                                case "7b":
                                    if (b[i])
                                        tmp |= 0x01;
                                    if (b[i + 1])
                                        tmp |= 0x02;
                                    if (b[i + 2])
                                        tmp |= 0x04;
                                    if (b[i + 3])
                                        tmp |= 0x08;
                                    if (b[i + 4])
                                        tmp |= 0x10;
                                    if (b[i + 5])
                                        tmp |= 0x20;
                                    if (b[i + 6])
                                        tmp |= 0x40;
                                    i += 7;
                                    tmp = (byte)(tmp * (255 / 127));
                                    break;

                                case "8b":
                                    if (b[i])
                                        tmp |= 0x01;
                                    if (b[i + 1])
                                        tmp |= 0x02;
                                    if (b[i + 2])
                                        tmp |= 0x04;
                                    if (b[i + 3])
                                        tmp |= 0x08;
                                    if (b[i + 4])
                                        tmp |= 0x10;
                                    if (b[i + 5])
                                        tmp |= 0x20;
                                    if (b[i + 6])
                                        tmp |= 0x40;
                                    if (b[i + 7])
                                        tmp |= 0x80;
                                    i += 8;
                                    break;

                                case "5/6/5":
                                    if (j == 0 || j == 2)
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;
                                        if (b[i + 2])
                                            tmp |= 0x04;
                                        if (b[i + 3])
                                            tmp |= 0x08;
                                        if (b[i + 4])
                                            tmp |= 0x10;
                                        i += 5;
                                        tmp = (byte)(tmp * (255 / 31));
                                    }
                                    else
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;
                                        if (b[i + 2])
                                            tmp |= 0x04;
                                        if (b[i + 3])
                                            tmp |= 0x08;
                                        if (b[i + 4])
                                            tmp |= 0x10;
                                        if (b[i + 5])
                                            tmp |= 0x20;
                                        i += 6;
                                        tmp = (byte)(tmp * (255 / 63));
                                    }
                                    break;

                                case "3/3/2":
                                    if (j == 0 || j == 1)
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;
                                        if (b[i + 2])
                                            tmp |= 0x04;

                                        i += 3;
                                        tmp = (byte)(tmp * (255 / 7));
                                    }
                                    else
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;

                                        i += 2;
                                        tmp = (byte)(tmp * (255 / 3));
                                    }
                                    if (tmp != 0)
                                    {
                                        Debug.WriteLine(j + " " + tmp);
                                    }
                                    break;

                                case "3/2/3":
                                    if (j == 0 || j == 2)
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;
                                        if (b[i + 2])
                                            tmp |= 0x04;

                                        i += 3;
                                        tmp = (byte)(tmp * (255 / 7));
                                    }
                                    else
                                    {
                                        if (b[i])
                                            tmp |= 0x01;
                                        if (b[i + 1])
                                            tmp |= 0x02;

                                        i += 2;
                                        tmp = (byte)(tmp * (255 / 3));
                                    }
                                    break;

                                case "Niestandardowy":
                                    
                                    for(int z1 =0; z1<parts[j]; z1++){
                                        if(b[i+z1]){
                                            tmp |= (byte)(0x01 << z1);
                                        }
                                    }
                                            
                                    i += parts[j];

                                    tmp = (byte) (tmp * (255 / Math.Pow(2,parts[j])-1));
                                    
                                    break;

                                default:
                                    MessageBox.Show("Błąd programu");
                                    return;
                            }
                            arr[j] = tmp;
                            i += paddingInternalLen;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Przekroczono koniec wczytanych danych");
                            i += 8;
                            break;
                        }
                    }
                    i += paddingLen;

                    // Create color
                    System.Drawing.Color color = System.Drawing.Color.White;

                    switch (((ComboBoxItem)ColorPallete.SelectedItem).Content.ToString())
                    {
                        case "RGB":
                            color = System.Drawing.Color.FromArgb(arr[0], arr[1], arr[2]);
                            break;

                        case "RGBA":
                            color = System.Drawing.Color.FromArgb(arr[3],arr[0], arr[1], arr[2]);
                            break;

                        case "ARGB":
                            color = System.Drawing.Color.FromArgb(arr[0], arr[1], arr[2], arr[3]);
                            break;

                        case "GBR":
                            color = System.Drawing.Color.FromArgb(arr[1], arr[2], arr[0]);
                            break;

                        case "BGR":
                            color = System.Drawing.Color.FromArgb(arr[2], arr[1], arr[0]);
                            break;

                        case "BRG":
                            color = System.Drawing.Color.FromArgb(arr[2], arr[0], arr[1]);
                            break;

                        case "GRB":
                            color = System.Drawing.Color.FromArgb(arr[1], arr[0], arr[2]);
                            break;
                    }
                    if (pixel > bmp.Width * bmp.Height)
                    {
                        MessageBox.Show("Program dotarł do granicy bitmapy przy nadal istniejących danych binarnych. Zbyt niska rozdzielczość?");
                        break;
                    }

                    if ((bool)Negative.IsChecked)
                    {
                        color = System.Drawing.Color.FromArgb(color.A, 255-color.R, 255-color.G, 255-color.B);
                    }

                    // Set pixel
                    int x = pixel%bmp.Width;
                    int y = pixel/bmp.Width;
                    pixel++;

                    if ((bool)Log.IsChecked)
                    {
                        file.WriteLine(x.ToString() + "," + y.ToString() + ":" + color.ToString() + " [" + pixel.ToString() +"/"+i.ToString()+"]");
                    }

                    if (x < bmp.Width && y < bmp.Height)
                        bmp.SetPixel(x, y, color);

                    else
                    {
                        //MessageBox.Show(x.ToString() + " " + y.ToString() + " " + pixel);
                        break;
                    }
                }

                // Show image
                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                  bmp.GetHbitmap(),
                  IntPtr.Zero,
                  System.Windows.Int32Rect.Empty,
                  BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));

                ImagePanel.Source = bs;
            }
            else
            {
                MessageBox.Show("Brak otwartego pliku");
            }
        }

        private void SaveBMP_Click(object sender, RoutedEventArgs e)
        {
            Save(".png");
        }


        private void Save(string extension)
        {
            if (bmp != null && TextBoxOutputFile.Text.Length > 0)
            {
                if(TextBoxOutputFile.Text.EndsWith(".png"))
                    bmp.Save(TextBoxOutputFile.Text );
                else
                    bmp.Save(TextBoxOutputFile.Text+extension);
                MessageBox.Show("Plik zapisany");
            }
            else
            {
                MessageBox.Show("Brak bitmapy lub podanej nazwy pliku");
            }
        }

        private void OutFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".png";
            dlg.Filter= "Plik PNG|.png";
            dlg.FilterIndex = 1;

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                TextBoxOutputFile.Text = dlg.FileName;
                file.WriteLine("Open " + dlg.FileName);
            }
        }

        private void ImagePanel_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (scale + e.Delta / 500.0f > 1.0)
            {
                scale += e.Delta / 300.0f;
            }
            else
            {
                scale = 1;
            }

            System.Windows.Controls.Image panel = (System.Windows.Controls.Image)sender;

            panel.ClipToBounds = true;
            ImageBorder.ClipToBounds = true;

            TransformGroup group = new TransformGroup();

            ScaleTransform scalet = new ScaleTransform();
            scalet.ScaleX = scale;
            scalet.ScaleY = scale;

            TranslateTransform translatet = new TranslateTransform();
            translatet.X = (ImageBorder.Width / 2)-(e.GetPosition(ImageBorder).X);
            translatet.Y = (ImageBorder.Height/ 2) - (e.GetPosition(ImageBorder).Y);
            
            group.Children.Add(scalet);
            //group.Children.Add(translatet);
            
            ImagePanel.RenderTransform = group;
        }

        private void WidthSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        { 
            if (buffer != null){
                Slider s = (Slider)sender;
                ImageWidth.Text = ((int)s.Value).ToString();
                TextBoxIHeader.Text = "0";
                ProcessImage_Click(null, null);
            }
        }

        private void LoadRLE_Click(object sender, RoutedEventArgs e)
        {
            if (buffer != null)
            {
                int zeros = 0;
                // Ignore header bytes
                buffer = buffer.Skip(Int32.Parse(TextBoxIHeader.Text)).ToArray();

                // Ignore footer bytes
                buffer = buffer.Take(buffer.Length - Int32.Parse(TextBoxIFooter.Text)).ToArray();

                List<byte> newBuffer = new List<byte>();
                for (int i = 0; i+2 < buffer.Length; )
                {
                    byte count = buffer[i];
                    byte color1 = buffer[i + 1];
                    byte color2 = buffer[i + 2];

                    if (count == 0) zeros++;

                    for (int j = 0; j < count; j++)
                    {
                        newBuffer.Add(color1);
                        newBuffer.Add(color2);
                    }
                    i += 3;
                }

                if (zeros > 0) MessageBox.Show("Kodowanie błędne, znaleziono " + zeros.ToString() + " zerowych wartości.");

                buffer = newBuffer.ToArray();
                FileSize.Text = buffer.Length.ToString();
            }
        }
    }
}
