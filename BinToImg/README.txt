Otw�rz plik - Wybiera �cie�k� do pliku.
Wczytaj plik - Wczytuje ca�y plik do programu (max oko�o 2GB)
Rozmiar - Ilo�� wczytanych danych
Nag��wek - Tymczasowo nieu�ywany
Format danych - [R]ed [G]reen [B]lue [A]lpha
D�ugo�� sk�adowej - Ilo�� bit�w sk�adaj�ca si� na jeden kolor, pomno�one przez liczb� sk�adowych (3 lub 4) daje rozmiar jednego pixela, np. dla 8b RGB jest to 24b.
Szerokos� i wysoko�� - rozdzielczo�� tworzonego obrazu.
Zignoruj nag��wek/stopk� - KA�DORAZOWO przy konwersji ta cz�� wczytanych danych jest ucinana z pocz�tku i z ko�ca.
Przesuni�cie pocz�tku - od kt�rego bitu od pocz�tku ma by� rozpocz�te wczytywanie (np. dla danych z oscyloskopu przesni�tych o 1-7 bit�w).
Padding - liczba danych ignorowanana ka�dorazowo po wczytaniu pixela, np:
[RRRRR GGGGG BBBBB PPP] <- wczytana ramka pixela dla paddingu 3
Negatyw - odwr�cenie kolor�w.
Zapis logu - czy informacje o pozycjach kolor�w maj� zosta� zapisane do pliku log.txt w katalogu programu.
Konwertuj na obraz - rozpoczyna konwersj�.

Zapisz obraz zapisuje w podanej lokalizacji i w wybranym formacie.

Enjoy.

Icon: http://www.yootheme.com/icons